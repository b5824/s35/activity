const express = require('express');
const router = express.Router();
//Router method - allows access to HTTP methods

const taskControllers = require('../controllers/taskControllers');

console.log(taskControllers);

//create task route. At the moment, we don't have direct access to the server, hence, we NEED to use router in order to have access to the HTTP methods
router.post('/', taskControllers.createTaskController);
router.get('/', taskControllers.getAllTasksController);

module.exports = router;