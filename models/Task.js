const mongoose = require('mongoose');

/* 
    Models by convention always starts with a capital letter.

    Schema - A blueprint for out data / document. Schema needs to know what needs to be built
*/
const taskSchema = new mongoose.Schema({
    name: String,
    status: String,
});

/* 
!    Mongoose model goes WITH the schema declared above

?        Syntax:
*            mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)

*            module.exports - Allows us to use the model outside of the file. Each file we have are called modules.
*            model - is used for manipulating objects or manipulating the data or document. Therefore, we need to export it and import it in the logic or the controllers. This holds the document that is about to be formed or created.
*/
//We're technically exporting the documents or collections that's about to be formed.
//It already holds the documents and we're exporting it to be manipulated
//Ensure that the model is singular because once it is saved in the database, it is pluralised.
module.exports = mongoose.model('Task', taskSchema);