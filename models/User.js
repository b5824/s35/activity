/* 
    >> Create a new schema for User. It should have the following fields:
        --username,
        --password
    The data types for both fields is String.

    >> Create a new model out of your schema and save it in a variable called User
*/
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: String,
    password: String,
});

module.exports = mongoose.model('User', userSchema);