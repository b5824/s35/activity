/* 
    ODM---
!    Mongoose
*        -Library that manages data "relationships", "validates schemas",  and simplifies MongoDB document "manipulation" via the use of "models"
    
?        Shcemas
*            -A schema is a representation of document's structure. It also contains document's expected properties and data types. Basically, schema is a blueprint, it is what the users have to follow when creating a document. This is like a contructor that can be created over and over again.

?        Models
*            -A programming interface for querying or manipulating a database. A Mongoose model contains methods that simplify such operations.

!    Separation of Concerns
*            - We separate moodules and parameterized routes such "Model", "Controllers" and "Routes" are divided into different folders. It is a must to separate them as it improves scalability, improves code readability and better code maintainability.

?                Model
                    - It is the absolute structure of how data is manipulated. It contains not just the properties but also the data type of data that needs to be received.
                
?                Controller
                    -This holds the business logic, the process. If a certain route is triggered, it is what we should execute. It contains instructions HOW your API will perform its intended tasks. 
                
?                Routes
                    -These are the endpoints. If this is triggered, the controller that goes with the route is then executed. Routes is when a controller will be used.

?                        Modules 
                            -These are self-contained units of functionality that can be shared and reused.
*/
//! Required Modules
const express = require('express');

//* This allows us to connect in our mongoDB
const mongoose = require('mongoose');

//! Port
const port = 4000;

//! Server
const app = express();

/* 
!   Mongoose Connection
        -We need to get the SRV string from mongo db (https://account.mongodb.com/account/login). In order to connect, go to the site and do the following, click "Connect" > Copy the connection string
*/
mongoose.connect(
    //mongodb+srv://CGarcia:admin169@cluster0.3t9w2.mongodb.net/tasks182?retryWrites=true&w=majority
    'mongodb+srv://admin:admin@wdc028-course-booking.j89li.mongodb.net/tasks182?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);
//This will create a notification if the db connection is successful or not.
let db = mongoose.connection;
/* 
    .on() - Used in the data step and end. In the event of having an error, execute the next params.
    console.error.bind() - This will show error in both terminal and in browser.
    .bind() - The bind() method creates a new function that, when called, has its this keyword set to the provided value, with a given sequence of arguments preceding any provided when the new function is called.

    once vs on:

    when you use 'once' it signifies that the event will be called only once i.e the first time the event occurred like here in this case the first time when the connection is opened ,it will not occur once per request but rather once when the mongoose connection is made with the db

    while the 'on' signifies the event will be called every time that it occurred
*/
db.on('error', console.error.bind(console, 'DB Connection Error'));

db.once('open', () => console.log('Successfully connected to MongoDB'));

//! Middleware
app.use(express.json());
//* This middleware takes place in forms, it is used to read data forms. The data we normally get from a form is in a form of string or array data type. What this middleware is doing is that, it accepts ALL data types such as: objects, numbers on top of the string and array.
app.use(express.urlencoded({ extended: true }));

//! Routes - Grouping routes
const taskRoutes = require('./routes/taskRoutes');
app.use('/tasks', taskRoutes);

const userRoutes = require('./routes/usersRoutes');
app.use('/users', userRoutes);

app.listen(port, () => console.log(`Listening to port: ${port}`));