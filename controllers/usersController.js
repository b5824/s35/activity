/* 
   >> Create a new POST method route to create 3 new user documents:
        -endpoint: "/users"
        -This route should be able to create a new user document.
        -Then, send the result in the client.
        -Catch an error while saving, send the error in the client.
        -STRETCH GOAL: no duplicate usernames
*/
const User = require('../models/User');

module.exports.createUsers = (req, res) => {
    console.log(req.body);

    User.findOne({ username: req.body.username })
        .then((result) => {
            // console.log(result);
            if (result !== null && result.username === req.body.username) {
                res.send('User already exists!');
            } else {
                let createNewUser = new User({
                    username: req.body.username,
                    password: req.body.password,
                });

                createNewUser
                    .save()
                    .then((result) => res.send(result))
                    .catch((err) => res.send(err));
            }
        })
        .catch((err) => res.send(err));
};